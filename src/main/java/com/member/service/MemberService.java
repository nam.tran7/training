package com.member.service;

import com.auth.Auth;
import com.auth.service.IAuthService;
import com.member.Member;
import com.member.MemberRepository;
import com.member.command.CommandAddMember;
import com.member.command.CommandUpdateMember;
import com.utils.ExceptionEnum;
import com.utils.HashUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class MemberService implements IMemberService {
    @Inject
    private MemberRepository memberRepository;
    @Inject
    private IAuthService authService;
    @Inject
    private JsonWebToken jwt;

    @Override
    public List<Member> getAll() {
        return memberRepository.listAll();
    }

    @Override
    @Transactional
    public Member add(CommandAddMember command) throws Exception {
        if (StringUtils.isAnyBlank(command.getName(), command.getEmail(), command.getRole(), command.getPassword())) {
            throw new Exception(ExceptionEnum.param_not_null);
        }
        if (memberRepository.checkEmail(command.getEmail())) {
            throw new Exception(ExceptionEnum.email_exist);
        }
        Member member = Member.builder()
                .email(command.getEmail())
                .name(command.getName())
                .role(command.getRole())
                .build();
        member.persist();
        Auth auth = Auth.builder()
                .member(member)
                .password(HashUtils.getPasswordMD5(command.getPassword()))
                .build();
        auth.persist();
        return member;
    }

    @Override
    @Transactional
    public Member update(CommandUpdateMember command) throws Exception {
        if (command.getId() == null) {
            throw new Exception(ExceptionEnum.param_not_null);
        }
        if (!jwt.getGroups().contains(Member.TYPE.admin) && command.getId() != Long.parseLong(jwt.getClaim("id").toString())) {
            throw new Exception(ExceptionEnum.member_type_deny);
        }
        Member member = memberRepository.findById(command.getId());
        if (member == null) {
            throw new Exception(ExceptionEnum.member_not_exist);
        }
        if (StringUtils.isNotBlank(command.getName())) {
            member.setName(command.getName());
        }
        if (StringUtils.isNotBlank(command.getEmail()) && !command.getEmail().equals(member.getEmail())) {
            if (memberRepository.checkEmail(command.getEmail())) {
                throw new Exception(ExceptionEnum.email_exist);
            }
            member.setEmail(command.getEmail());
        }
        member.persist();
        return member;
    }

    @Override
    @Transactional
    public Boolean delete(Long id) throws Exception {
        Auth auth = authService.getByMemberId(id);
        if (auth == null) {
            throw new Exception(ExceptionEnum.member_not_exist);
        }
        auth.delete();
        Member member = memberRepository.findById(id);
        if (member == null) {
            throw new Exception(ExceptionEnum.member_not_exist);
        }
        member.delete();
        return Boolean.TRUE;
    }

    @Override
    public Member findByEmail(String email) {
        return memberRepository.find("email", email).firstResult();
    }
}
