package com.utils;

import io.quarkus.security.identity.CurrentIdentityAssociation;
import io.quarkus.security.identity.SecurityIdentity;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
@Priority(Priorities.USER)
public class SecurityIdentityHolder implements ContainerRequestFilter {
    private static final ThreadLocal<SecurityIdentity> SECURITY_IDENTITY_HOLDER = new InheritableThreadLocal<>();

    @Inject
    CurrentIdentityAssociation association;

    public static SecurityIdentity getIdentity() {
        return SECURITY_IDENTITY_HOLDER.get();
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        SECURITY_IDENTITY_HOLDER.set(association.getIdentity());
    }
}
