package com.common;

import javax.ws.rs.core.Response;

public class HTTPResponse {
    public Response success(Object data) {
        return Response.ok(BodyResponse.builder()
                .code(9999)
                .payload(data)
                .build()).build();
    }

    public Response error(String message) {
        return Response.status(Response.Status.BAD_REQUEST).entity(BodyResponse.builder()
                .message(message)
                .code(-9999)
                .build()).build();
    }
}
