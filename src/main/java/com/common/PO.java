package com.common;

import com.utils.SecurityIdentityHolder;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.security.identity.SecurityIdentity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@MappedSuperclass
@Getter
@Setter
@EntityListeners(PO.AuditListener.class)
public class PO extends PanacheEntityBase {
    @Column(name = "create_date")
    private Long createDate;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "last_update_date")
    private Long lastUpdateDate;
    @Column(name = "last_update_by")
    private String lastUpdateBy;

    public static class AuditListener {
        private String currentIdentityName() {
            SecurityIdentity identity = SecurityIdentityHolder.getIdentity();
            return identity != null && !"".equals(identity.getPrincipal().getName()) ? identity.getPrincipal().getName() : "system";
        }

        @PrePersist
        private void forCreate(PO entity) {
            long now = System.currentTimeMillis();
            String name = currentIdentityName();
            entity.createDate = now;
            entity.createBy = name;
            entity.lastUpdateDate = now;
            entity.lastUpdateBy = name;
        }

        @PreUpdate
        private void forUpdate(PO entity) {
            entity.lastUpdateDate = System.currentTimeMillis();
            entity.lastUpdateBy = currentIdentityName();
        }
    }
}
