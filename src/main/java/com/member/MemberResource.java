package com.member;

import com.common.HTTPResponse;
import com.member.command.CommandAddMember;
import com.member.command.CommandUpdateMember;
import com.member.service.IMemberService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/member")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MemberResource extends HTTPResponse {
    @Inject
    IMemberService memberService;

    @GET
    @RolesAllowed("admin")
    public Response getAll() {
        return success(memberService.getAll());
    }

    @POST
    public Response add(CommandAddMember command) {
        try {
            return success(memberService.add(command));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    @RolesAllowed({"admin", "customer"})
    public Response update(CommandUpdateMember command, @PathParam("id") Long id) {
        try {
            command.setId(id);
            return success(memberService.update(command));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed("admin")
    public Response delete(@PathParam("id") Long id) {
        try {
            return success(memberService.delete(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }
}
