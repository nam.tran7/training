package com.member.service;

import com.member.Member;
import com.member.command.CommandAddMember;
import com.member.command.CommandUpdateMember;

import javax.transaction.Transactional;
import java.util.List;

public interface IMemberService {
    List<Member> getAll();

    @Transactional
    Member add(CommandAddMember command) throws Exception;

    @Transactional
    Member update(CommandUpdateMember command) throws Exception;

    @Transactional
    Boolean delete(Long id) throws Exception;

    Member findByEmail(String email);
}
