package com.member;

import com.common.PO;
import lombok.*;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.*;

@Entity
@Setter
@Getter
@ApplicationScoped
@Table(name = "member")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Member extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private String role;

    public static class TYPE {
        public static final String admin = "admin";
        public static final String customer = "customer";
    }
}
