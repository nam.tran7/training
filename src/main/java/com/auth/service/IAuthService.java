package com.auth.service;

import com.auth.Auth;
import com.auth.command.CommandChangePassword;
import com.auth.command.CommandLogin;

import javax.transaction.Transactional;

public interface IAuthService {
    String login(CommandLogin command) throws Exception;

    Auth getByMemberId(Long id);

    @Transactional
    Boolean changePassword(CommandChangePassword command) throws Exception;
}
