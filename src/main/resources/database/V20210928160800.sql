CREATE TABLE member
(
    id  SERIAL PRIMARY KEY,
    name VARCHAR(100),
    email VARCHAR(100),
    create_date BIGINT,
    last_update_date BIGINT,
    create_by VARCHAR(100),
    last_update_by VARCHAR(100),
    role VARCHAR(10) default 'customer'
);
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by) VALUES ('test1', 'test1@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by) VALUES ('test2', 'test2@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by) VALUES ('test3', 'test3@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by, role) VALUES ('test3', 'admin@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin', 'admin');