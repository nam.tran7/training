package com.auth;

import com.common.PO;
import com.member.Member;
import lombok.*;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.*;

@Entity
@Setter
@Getter
@ApplicationScoped
@Table(name = "auth")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Auth extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String password;
    @OneToOne
    @JoinColumn(name = "member_id", nullable = false)
    private Member member;
}
