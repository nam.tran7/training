CREATE TABLE member
(
    id  SERIAL PRIMARY KEY,
    name VARCHAR(100),
    email VARCHAR(100),
    create_date BIGINT,
    last_update_date BIGINT,
    create_by VARCHAR(100),
    last_update_by VARCHAR(100),
    role VARCHAR(10) default 'customer'
);
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by) VALUES ('test1', 'test1@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by) VALUES ('test2', 'test2@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by) VALUES ('test3', 'test3@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO member(name, email, create_date, last_update_date, create_by, last_update_by, role) VALUES ('admin', 'admin@gmail.com', 1632822838000, 1632822838000, 'admin', 'admin', 'admin');

CREATE TABLE auth
(
    id SERIAL PRIMARY KEY,
    member_id BIGINT REFERENCES member,
    password VARCHAR (40),
    create_date BIGINT,
    last_update_date BIGINT,
    create_by VARCHAR(100),
    last_update_by VARCHAR(100)
);

INSERT INTO auth(member_id, password, create_date, last_update_date, create_by, last_update_by) VALUES (1, 'f6fdffe48c908deb0f4c3bd36c032e72', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO auth(member_id, password, create_date, last_update_date, create_by, last_update_by) VALUES (2, 'f6fdffe48c908deb0f4c3bd36c032e72', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO auth(member_id, password, create_date, last_update_date, create_by, last_update_by) VALUES (3, 'f6fdffe48c908deb0f4c3bd36c032e72', 1632822838000, 1632822838000, 'admin', 'admin');
INSERT INTO auth(member_id, password, create_date, last_update_date, create_by, last_update_by) VALUES (4, 'f6fdffe48c908deb0f4c3bd36c032e72', 1632822838000, 1632822838000, 'admin', 'admin');