package com.auth.service;

import com.auth.Auth;
import com.auth.AuthRepository;
import com.auth.command.CommandChangePassword;
import com.auth.command.CommandLogin;
import com.member.Member;
import com.utils.ExceptionEnum;
import com.utils.HashUtils;
import io.smallrye.jwt.build.Jwt;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@ApplicationScoped
public class AuthService implements IAuthService {
    @Inject
    private AuthRepository authRepository;
    @Inject
    private JsonWebToken jwt;

    @Override
    public String login(CommandLogin command) throws Exception {
        if (StringUtils.isAnyBlank(command.getEmail(), command.getPassword())) {
            throw new Exception(ExceptionEnum.param_not_null);
        }
        EntityManager entityMgr = authRepository.getEntityManager();
        String queryStr = "select u from Auth u where u.member.email = :email";
        TypedQuery<Auth> typedQuery = entityMgr.createQuery(queryStr, Auth.class).setParameter("email", command.getEmail());
        List<Auth> list = typedQuery.getResultList();
        if (CollectionUtils.isEmpty(list)) {
            throw new Exception(ExceptionEnum.member_not_exist);
        }
        Auth auth = list.get(0);
        if (!HashUtils.getPasswordMD5(command.getPassword()).equals(auth.getPassword())) {
            throw new Exception(ExceptionEnum.password_incorrect);
        }
        return createSession(auth.getMember());
    }

    private String createSession(Member member) {
        return Jwt.issuer("http://localhost:8080")
                .upn(member.getEmail())
                .groups(Collections.singleton(member.getRole()))
                .claim("id", member.getId())
                .expiresAt((System.currentTimeMillis() + (30 * 30 * 6000)) / 1000)
                .sign();
    }

    @Override
    public Auth getByMemberId(Long id) {
        return authRepository.find("member_id", id).firstResult();
    }

    @Override
    @Transactional
    public Boolean changePassword(CommandChangePassword command) throws Exception {
        if (StringUtils.isAnyBlank(command.getPassword(), command.getPasswordConfirm()) || command.getId() == null) {
            throw new Exception(ExceptionEnum.param_not_null);
        }
        if (!jwt.getGroups().contains(Member.TYPE.admin) && command.getId() != Long.parseLong(jwt.getClaim("id").toString())) {
            throw new Exception(ExceptionEnum.member_type_deny);
        }
        if (!command.getPassword().equals(command.getPasswordConfirm())) {
            throw new Exception(ExceptionEnum.password_confirm_incorrect);
        }
        Auth auth = authRepository.findById(command.getId());
        if (auth == null) {
            throw new Exception(ExceptionEnum.member_not_exist);
        }
        auth.setPassword(HashUtils.getPasswordMD5(command.getPassword()));
        auth.persist();
        return Boolean.TRUE;
    }
}
