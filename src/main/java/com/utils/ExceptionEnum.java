package com.utils;

public class ExceptionEnum {
    public static final String param_not_null = "param not null";
    public static final String member_exist = "member exist";
    public static final String member_not_exist = "member not exist";
    public static final String email_exist = "email exist";
    public static final String password_incorrect = "password incorrect";
    public static final String password_confirm_incorrect = "password confirm incorrect";
    public static final String member_type_deny = "member type deny";
}
