package com.member;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MemberRepository implements PanacheRepository<Member> {
    public Boolean checkEmail(String email) {
        return this.count("email", email) > 0;
    }
}
