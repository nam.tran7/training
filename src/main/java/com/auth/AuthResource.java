package com.auth;

import com.auth.command.CommandChangePassword;
import com.auth.command.CommandLogin;
import com.auth.service.IAuthService;
import com.common.HTTPResponse;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthResource extends HTTPResponse {
    @Inject
    IAuthService authService;

    @POST
    @Path("/login")
    @PermitAll
    public Response login(CommandLogin command) {
        try {
            return success(authService.login(command));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    @PUT
    @Path("/change_password/{id}")
    @RolesAllowed({"admin", "customer"})
    public Response changePassword(CommandChangePassword command, @PathParam("id") Long id) {
        try {
            command.setId(id);
            return success(authService.changePassword(command));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }
}
